site_name: Open Quantum Sensing and Measurement Notes
repo_url: https://gitlab.kwant-project.org/quantum-sensing-and-measurement/quantum-sensing-and-measurement
repo_name: source
edit_uri: edit/master/src/
site_description: |
  Lecture notes for the TU Delft course TN3155 Quantum Sensing and Measurement

nav:
  - Intro: 'index.md'
  - Week 1, Classical Noise:
      - Basics of Noise: '1_basics_of_noise.md'
      - Noise Processes and Measurement Sensitivity: '2_noise_types.md'
      - Noise in Harmonic Oscillators: '3_noise_in_HOs.md'
  - Week 2, The Noise of Quantum States:
      - Zero-Point Fluctuations: '4_ZPFs.md'
      - Wigner Functions: 
        - Definition of the Wigner Function: '5_wigner.md'
        - Wigner functions in QuTiP: '5_wigner_2.md'
      - Quantum Backaction: '6_backaction.md'
  - Week 3, Time Evolution of Quantum States:
      - The Density Matrix: '7_density.md'
      - Wigner Functions of Mixed States: '7_mixed_states.md'
      - The Lindblad Master Equation: '8_lindblad.md'
#      - Introduction to QuTip: '9_qutip.md'


theme:
  name: material
  custom_dir: theme
  palette:
    primary: 'white'
    accent: 'indigo'

markdown_extensions:
  - mdx_math:
      enable_dollar_delimiter: True
  - toc:
      permalink: True
  - admonition
  - pymdownx.details
  - pymdownx.extra
  - abbr
  - footnotes
  - meta
  - attr_list

extra_css:
  - 'https://use.fontawesome.com/releases/v5.8.1/css/all.css'
  - 'styles/thebelab.css'

extra_javascript:
  - 'https://cdn.plot.ly/plotly-latest.min.js'
  - 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.3/MathJax.js?config=TeX-AMS_HTML'
  - 'scripts/mathjaxhelper.js'
  - 'scripts/x3dom.js'
copyright: "Copyright © 2017-2019 Delft University of Technology, CC-BY-SA 4.0."
