# Quantum sensing and measurement

!!! summary "Learning goals"

    After following this course you will be able to:

    - characterize noise in classical and quantum measurements
    - describe the time evolution of quantum states in noisy environments
    - analyze the dynamics of quantum systems using QuTiP

In these notes our aim is to provide learning materials which are:

- self-contained
- easy to modify and remix, so we provide the full source, including the code
- open for reuse: see the license below.

Whether you are a student taking this course, or an instructor reusing the materials, we welcome all contributions, so check out the [course repository](https://gitlab.kwant-project.org/quantum-sensing-and-measurement/quantum-sensing-and-measurement), especially do [let us know](https://gitlab.kwant-project.org/quantum-sensing-and-measurement/quantum-sensing-and-measurement/issues/new?issuable_template=typo) if you see a typo!
