---
jupyter:
  jupytext:
    formats: md,ipynb
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.10.1
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python
import pandas

from matplotlib import pyplot
import numpy as np
from scipy.optimize import curve_fit
from scipy.integrate import quad
import plotly.offline as py
import plotly.graph_objs as go


```

# Lecture 7: The Density Matrix

!!! summary "Learning goals"

    After this lecture you will be able to:

    - Do cool things
    

Last lecture, we learned about continuous (i.e. weak) measurements in QM, which we can summarize as follows

1. In continuous measurements, collapse events occur randomly in time, following a Poisson distribution

2. At each collapse event, we apply the usual recipe for quantum measurement

3. The average rate at which the collapse events occur is proportional to how fast we (or other people) extract information about the observable we are observing

4. These collapse events result in "quantum backation" that disturbs the state we are measuring and can (but does not always) add noise to the observable we are measuring

We then saw a few examples

1. Noise in the oscillations of $\langle S_x(t) \rangle$ of a spin when measuring $S_x$

2. Noise in $\langle x(t) \rangle$ of a harmonic oscillator induced by measurements of $x$

Another example: measure $\langle n \rangle$ of a coherent state

<!-- <div align="center"><img src="figures/7/poisson_meas.PNG" alt="" width="700px"></div> -->
![](figures/7/poisson_meas.PNG){style="width:500px}

After the first collapse, $\langle n \rangle$ no longer changes in subsequent collapses.  The reason is because the number operator $\hat{n}$ commutes with the Hamiltonian: $[\hat{n}, \hat{H}] = 0$.  Thus, they share an eigenbasis, and upon measurement the system collapses to a stationary state, where $\langle n \rangle$ is not time-dependent.  This is an example of a quantum non-demolition experiment. The first measurment always collapses $|\psi\rangle$, but the next measurements don't cause more "damage"; as a result, you can average stationary quantities for a long time, say, if your detector adds a lot of classical noise.

But there is a problem: what if we miss some of the measurement results, or our detector is noisy, giving us only *partial* information?  Or even none at all?  

<!-- <div align="center"><img src="figures/7/mcwf.PNG" alt="" width="400px"></div> -->
![](figures/7/mcwf.PNG){style="width:350px}

If our lack of information makes it ambiguous as to what state the system is in, then should we then keep track of two Schrodinger equations (like in the image)?  Actually, in such a case, we would need to average over different collapse events in time, which is exactly what the Monte Carlo Wave Function (MCWF) method does, which we will see later in QuTip.

However, if we are only interested in the *average* result over different quantum trajectories, then we can just use the density matrix formalism.

The density matrix is defined as

$$
\rho = |\psi\rangle\langle\psi|
$$

where $|\psi\rangle$ is some state (column) vector.  We recall the usual inner product between state vectors

$$
\langle\psi|\psi\rangle = \sum_i |c_i|^2
$$

which is a scalar quantity.  But what does it mean to reverse this notation, as in the definition of a density matrix?  Writing $|\psi\rangle\langle\psi|$ is called an outer product, and instead of a scalar, it is a matrix:

$$
|\psi\rangle\langle\psi| = 
\begin{bmatrix}
c_1 \\
c_2 \\
c_3 \\
\vdots
\end{bmatrix}
\begin{bmatrix}
c_1^* & c_2^* & c_3^* & \dots 
\end{bmatrix} =
\begin{bmatrix}
c_1 c_1^* & c_1 c_2^* & c_1 c_3^* & \dots \\
c_2 c_1^* & c_2 c_2^* & c_2 c_3^* & \dots \\
c_3 c_1^* & c_3 c_2^* & c_3 c_3^* & \dots \\
\vdots & \vdots & \vdots & \ddots \\
\end{bmatrix}
$$

Some properties of $\rho$:

$$
\begin{aligned}
Tr(\rho) &= \sum_i |c_i|^2 = 1\\
\rho_{ij} &= \rho_{ji} \quad (\text{Hermitian})
\end{aligned}
$$

The typical example is spin-$1/2$:

$$
\begin{align}
|\psi\rangle &= |\uparrow\rangle = \begin{bmatrix} 1 \\ 0 \end{bmatrix} \rightarrow
\rho = \begin{bmatrix} 1 & 0 \\ 0 & 0 \end{bmatrix}
\rightarrow 
Tr(\rho) = 1 + 0 = 0\\
|\psi\rangle &= |y+\rangle = \frac{1}{\sqrt{2}}\begin{bmatrix} 1 \\ i \end{bmatrix} \rightarrow
\rho = \begin{bmatrix} 1/2 & -i/2 \\ i/2 & 1/2 \end{bmatrix}
\rightarrow 
Tr(\rho) = 1/2 + 1/2 = 1
\end{align}
$$
    
Now we need to keep track of $N^2$ numbers in matrix instead of $N$ in a vector, so what is the benefit?  For one, there is a very handy expression for expectation values:

$$
\langle A \rangle = Tr(\rho A)
$$

A few examples of using this nice trace formula:

$$
\begin{align}
S_Z, |\uparrow\rangle &\rightarrow Tr \bigg( \frac{\hbar}{2}\begin{bmatrix} 1 & 0 \\ 0 & 0 \end{bmatrix} \bigg) = \frac{\hbar}{2}\\
S_Y, |\uparrow\rangle &\rightarrow Tr \bigg( \frac{\hbar}{2}\begin{bmatrix} 0 & i \\ 0 & 0 \end{bmatrix} \bigg) = 0\\
S_Z, |y+\rangle &\rightarrow Tr \bigg( \frac{\hbar}{4}\begin{bmatrix} 1 & -i \\ -i & -1 \end{bmatrix} \bigg) = 0\\
S_Y, |y+\rangle &\rightarrow Tr \bigg( \frac{\hbar}{4}\begin{bmatrix} 1 & -i \\ i & 1 \end{bmatrix} \bigg) = \frac{\hbar}{2}
\end{align} 
$$

The time-dependent Schrodinger equation (TDSE) also takes on a compact form:

$$
i\hbar \frac{\partial \rho}{\partial t} = [\hat{H}, \rho]
$$

But how does this help with the collapse problem?  The key concept is that $\rho$ can allow us to include *classical* uncertainty into our quantum calculations.

Let's say that we do not know the exact state of the quantum particle, but we know that there is a 50% chance it is in state $|\psi_1\rangle$ and 50% chance it is in $|\psi_2\rangle$.  The density matrix for this situation is

$$
\begin{align}
\rho &= \sum_i p_i |\psi_i\rangle\langle\psi_i|\\ &= 0.5|\psi_1\rangle\langle\psi_1| + 0.5 |\psi_2\rangle\langle\psi_2|
\end{align}
$$

This kind of density matrix is called a "mixed state" (or more appropriately, but less commonly, a "mixed ensemble"), in contrast to the case where there is only one term in the sum, which is called a pure state and was the first example we saw.

We can use the same formula above to calculate an "ensemble" expectation value of an observable:

$$
\langle A \rangle = Tr(\rho A)
$$

For pure states, this has the usual meaning of preparing a state $|\psi\rangle$, measuring the observable $\hat{A}$, and repeating this process to build up an average measurement result $\langle A \rangle$.

For mixed states, the process is a bit different.  We do not prepape the same state each time, since we need to import classical uncertainty into the process.  Each new $|\psi\rangle$ is either $|\psi_1\rangle$ or $|\psi_2\rangle$ with probability 50% for each (we can just flip a coin to decide).  The uncertainty in our measurement results has two distinct origins: there is fundamental "quantum" randomness from the collapse of the wavefunction, and also boring "classical" randomness from the coin flip that decides the state preparation.

Let's assume the mixed state is split 50-50 between spin-up and spin-down, that is, $|\psi_1\rangle =|\uparrow\rangle$ and $|\psi_2\rangle =|\downarrow\rangle$.  If we measure the observable $S_Z$, we would expect to get either up or down about half the time on average.

What if we had instead used the pure state $|+i\rangle\langle +i|$?  We would get the same measurment statistics!  But surely these states aren't the same, so how can we tell the difference?  The answer to this 
question is, at its core, what makes quantum *quantum*.

If we instead measure the observable $S_Y$, the mixed state will still yield 50-50 results because either of its two states can be written as superpositions between the $|\pm i \rangle$ eigenstates.  However, the pure state $|+i\rangle$ obviously will only ever give $+\hbar/2$ upon measuring the observable $S_Y$.  This difference can be visualized with the density matrix

$$
\begin{align}
0.5|\uparrow\rangle\langle\uparrow| + 0.5|\downarrow\rangle\langle\downarrow| &= 
\begin{bmatrix}
\frac{1}{2} & 0 \\
0 & \frac{1}{2}
\end{bmatrix}\\
|+i\rangle\langle +i| &= 
\begin{bmatrix}
\frac{1}{2} & \frac{-i}{2} \\
\frac{i}{2} & \frac{1}{2}
\end{bmatrix}
\end{align}
$$

where the diagonal terms are called weights or populations, and the off-diagonal terms are called coherences.  Mixing states suppresses the off-diagonal terms.  We can check how pure a state is by computing its "purity"

$$
\gamma = Tr(\rho^2)
$$

If the state is pure, then squaring $\rho$ won't change the fact that its trace is $1 + 0 = 1$.  But if some diagonal element is less than 1, then squaring the matrix will reduce the trace.

Lastly, the Wigner functions we saw earlier (for pure states) can *also* be used to encode classical uncertainty, like the density matrix.  If we have a density matrix (pure or mixed), then its Wigner function is

$$
W(x,p) = \frac{1}{\pi \hbar} \int_{-\infty}^{+\infty} \langle x+y | \rho | x-y \rangle e^{-2ipy/\hbar} dy
$$

where $|x\rangle$ are the eigenstates of the position operator, expressed in whatever basis was chosen for $\rho$.

From the above, we can see that Wigner functions of mixed states have an interesting property: unlike the Wigner functions of superposition states, the Wigner functions of mixed states *do* add:

$$
\begin{align}
\rho_{mix} &= \rho_1 + \rho_2\\
W_{mix} &= \frac{1}{\pi \hbar} \int_{-\infty}^{+\infty} \langle x+y | \rho_1 | x-y \rangle e^{-2ipy/\hbar} dy\\ &+ \frac{1}{\pi \hbar} \int_{-\infty}^{+\infty} \langle x+y | \rho_2 | x-y \rangle e^{-2ipy/\hbar} dy \\ &= W_1 + W_2
\end{align}
$$

# Homework Lecture 7

##  Exercise: Thermal state (ensemble) of a spin in a magnetic field

In this exercise, we will consider a negatively-charged spin-1/2 particle (such as an electron) in an external magnetic field in the $z$-direction, $\vec{B} = B\hat{z}$, at a temperature $T$. 

With the usual convention: 

$$
\left| \uparrow \right\rangle  = 
\begin{pmatrix}
1 \\
0
\end{pmatrix}
\hspace{0.25in}
\left| \downarrow \right\rangle  = 
\begin{pmatrix}
0 \\
1
\end{pmatrix}
$$

the Hamiltonian of the spin of an electron in this magnetic field is given by: 

$$
\hat{H} =  -g\mu_B B \hat S_z = 
\frac{\hbar \omega_L}{2}
\begin{pmatrix}
1 & 0 \\
0 & -1
\end{pmatrix}
$$

noting that the g-factor of an electron is $g = -2$. Note also the mega-negative-sign confusion! Important thing to remember is that for an electron spin in a $+z$ magnetic field, the Hamiltonian is:

$$
\hat{H} =  
\frac{\hbar \omega_L}{2}
\begin{pmatrix}
1 & 0 \\
0 & -1
\end{pmatrix}  = 
{\hbar \omega_L \over 2} \sigma_z
$$

where $\omega_L$ is the Larmor frequency and is a positive number.

If the only information I have about the spin is its temperature, then the density matrix representing my knowledge about the state of the spin is a "mixed state". From thermodynamics, we know the average "populations" of the levels of the spin, corresponding the the diagonal "population" matrix elements of the density matrix, are given by:

$$
\rho^{th}_{nn} = A e^{-\beta \Delta  E_n}
$$

where $\Delta E_n$ is the energy of state $n$ relative to the ground state, $\beta = \frac{1}{k_B T}$, and $A$ is a constant.  

This "state" of the spin (or again,  more accurately, this state of *your knowledge* about the spin) is called a "thermal state". Or, in more precise language, $\rho^{th}$ describes a thermal ensemble of spin particles, or equivalently an ensemble of repeated measurements of a single spin which undergoes a re-thermalisation  with the "bath" between each measurement.

**(a)** Find the coefficient $A$. 

**(b)** Write down density matrix $\rho^{th}$ for the spin-1/2 particle at temperature $T$. 

**(c)** Consider $k_B T = \hbar \omega_L$  where $\omega_L$ is the Larmor frequency. Calculate the  spin populations $\rho_{11}$ and $\rho_{22}$ of $\rho^{th}$.

**(d)** Use $\rho^{th}$ to calculate $\langle S_x \rangle$, $\langle S_y \rangle$ and $\langle S_z \rangle$ for the thermal state of the spin at temperature $T = \frac{\hbar \omega_L}{k_B}$  

**(e)** What temperature $T$ do you need to get the example from class of a 50/50 mixture of spin up and spin down? 


## Demonstration: Thermal state of a harmonic oscillator (or a large spin)

In the code below, we will explore the thermal state of a Harmonic oscillator in QuTiP. 

As we always do in a computer, will will truncate the Hilbert space to a maximum photon (Fock) number $N$. This approximation is reasonable a long as the states we consider do not have significant "population" of Fock states $\rho_{nn}$ for $n > N$. 

It is also interesting to note that while this truncation to Hilbert size $N$ is an approximation for the Harmonic oscillator, it does exactly describe a spin-N/2 particle in a magnetic field, which also has an evenly spaced energy spectrum, but which does have a fixed maximum number of levels.  

```python
from qutip import *
import numpy as np
import matplotlib.pyplot as plt
```

QuTiP fortunately has a function `thermal_dm(N,n)` that can generate the density matrix of a thermal state. It needs two argements: `N` is the size of the Hilbert space, and `n` is the thermal occupation number $\langle n \rangle = {\rm Tr} (\rho \hat a^\dagger )$. 

```python
N=30
therm = thermal_dm(N,5)
fig,ax = plt.subplots(1,2, figsize=(12,6))
plot_wigner(therm,fig=fig,ax=ax[0])
plot_fock_distribution(therm, fig=fig,ax=ax[1], unit_y_range=False);
```

A thermal state with no photons is just the ground state:

```python
N=30
therm = thermal_dm(N,0)
fig,ax = plt.subplots(1,2, figsize=(12,6))
plot_wigner(therm,fig=fig,ax=ax[0])
plot_fock_distribution(therm, fig=fig,ax=ax[1], unit_y_range=False);
```

We can also look at the probability distributions for different temperatures:

```python
N = 30
Ntemp  = 10
Npts = 100

Px = np.zeros([Npts,Ntemp])
T = np.linspace(0,5,Ntemp)

alpha_max = 7.5
x = np.linspace(-alpha_max,alpha_max,Npts)

for i in range(Ntemp):
    wig = wigner(thermal_dm(N,T[i]),x,x)
    Px[:,i] = np.sum(wig,axis=0)

plt.plot(x,Px);
plt.ylabel("Probability density")
plt.xlabel("x")
plt.title("Thermal occupation from 0 to 5 quanta");
```

And make a plot of the variance of the measurements vs thermal occupation number:

```python
from scipy.signal import peak_widths, find_peaks

fwhm = np.zeros(Ntemp)

for i in range(Ntemp):
    x=Px[:,i]
    p, _ = find_peaks(x)
    w = peak_widths(x, p)
    fwhm[i] = w[0]

fwhm /= fwhm[0]

plt.plot(fwhm**2)
plt.axhline(0,ls=":", c='grey')
plt.axvline(0,ls=":", c='grey')
plt.ylabel("$\sigma_x^2$ ($x_{ZPF}^2$)")
plt.xlabel("Thermal occupation (quanta)");
```

At high temperature, the uncertainty comes from the "classical" uncertainty from the thermal bath (Brownian motion), while at zero temperature, the residual remaining uncertainty comes from quantum fluctuations.

