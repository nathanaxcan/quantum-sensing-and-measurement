---
jupyter:
  jupytext:
    formats: md,ipynb
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.10.1
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python
import pandas

from matplotlib import pyplot
import numpy as np
from scipy.optimize import curve_fit
from scipy.integrate import quad
import plotly.offline as py
import plotly.graph_objs as go


```
# Lecture 4: Quantum fluctuations, the quantum harmonic oscillator, and coherent states

!!! success "Expected prior knowledge"

    Before the start of this lecture, you should be able to:

    - write down the energy spectrum of the quantum harmonic oscillator 
    - recall that the state of a quantum object is described by the wavefunction $|\Psi\rangle$
    - calculate the expectation value of an observable given a wavefunction

!!! summary "Learning goals"

    After this lecture you will be able to:

    - derive the quantum fluctuations for a given wavefunction
    - calculate expectation values of coherent states
    - show that the number of photons in a coherent state follows a Poisson distribution


So far, we have been reviewing concepts from classical noise, and discovering fun things like the fluctuation-dissipation theorem by accident.  This week, we will start to explore fluctuations in quantum mechanics.  We will start with a problem you have seen before: the quantum harmonic oscillator (i.e., a quantum mass on a spring).

![](figures/4/harmosc.png){style="width:500px"}

Why is the quantum harmonic oscillator such an important concept?  

- for small displacements from a minimum, all potentials are approximately harmonic (i.e., parabolic, via Taylor expansion)
- Electrical circuits can be harmonic oscillators
- light and lattice vibrations are descirbed by harmonic oscillators

The last one is particularly interesting, and we can gain a deeper understanding of what "photons" are by looking at how light, and photons, are related to harmonic oscillators. 

Imagine an empty box that contains electromagnetic radiation. Classically, each mode of the box is a standing wave that bounces up and down in time:

- mode $1$: $\quad E_1 \cos{(\omega_1 t)} \sin{(\frac{\pi x}{L})}$
- mode $2$: $\quad E_2 \cos{(\omega_2 t)} \sin{(\frac{2 \pi x}{L})}$
- mode $n$: $\quad E_n \cos{(\omega_n t)} \sin{(\frac{n \pi x}{L})}$

Associated with every electric field $\vec{E}$, there is a magnetic field $\vec{B}$ that oscillates out of phase with respect to the electric field (this follows from Maxwell's Equations).  The total energy is given by:

$$
E = \frac{1}{2}\int (\epsilon_0 |\vec{E}|^2 + \mu_0 |\vec{B}|^2) d^3 x
$$

For a given mode, this will be proportional to:

$$
E_m \propto a E_m^2 + b B_m^2 
$$

This is looking a lot like a harmonic oscillator

$$
E = \frac{1}{2} m \omega_0^2 x^2 + \frac{p^2}{2 m}
$$

where the electric and magnetic fields play the roles of position and momentum, respectively.

Each mode of the box can be mapped to an individual harmonic oscillator. Each such oscillator can be quantized, and each mode can be excited by an arbitrary number of photons.

![](figures/4/field_modes.PNG){style="width:500px"}

Having established the importance of the harmonic oscillator, let's look at the properties of some of its states.

- The ground state of the oscillator is the lowest-energy state allowed in quantum mechanics. Unlike in classical mechanics, the ground does not sit at the bottom of the potential.  Instead, it has a zero-point energy of $ \frac{1}{2}\hbar \omega $, where $\omega$ is the eigenfrequency of the oscilator.  

- The ground-state wavefunction is "smeared out" in space, so there are zero-point fluctuations in the position of the oscillator.  The standard deviation of the position is $x_\text{ZPF} = \sqrt{\frac{\hbar}{2 m \omega}}$.

What does it mean for the wavefunction to be "smeared out"?  Since we interpret $|\Psi(x)|^2$ as a probability distribution, we can take a particle described by the wavefunction, measure its position, and then reset the particle.  After many trials, the histogram of the random measurement results will converge to the distribution $\rho(x) = |\Psi(x)|^2$.

Let's take a step back, and pretend that we don't perform any measurements.  Does any "property" of the particle in the ground state of the quantum harmonic oscillator change in time?  

Under the evolution of the Schr\"{o}dinger equation, we have

$$
\Psi(x,t) = e^{i E_0 t / \hbar} \psi_0 (x)
$$

So the total wavefunction $\Psi(x,t)$ changes in time only via an oscillating phase factor. Do any observables change?  It turns out that none of $\langle x \rangle, \langle p \rangle, \langle E_\text{pot} \rangle, \langle E_\text{kin} \rangle$ change in time.  This is interesting, because if these quantities were associated with a classical oscillator, the finite energy would cause all of them to oscillate in time.  Instead, in the quantum case, the particle has energy but these expectation values are stationary.  Yes, there are fluctuations in the quantum sense, but these fluctuations themselves do not fluctuate in time. In fact, they only fluctuate if you measure them!  (A philosophical aside: does it even make sense to talk about something of we are not allowed to measure it?)

So, if the oscillator is stationary, why does it have kinetic energy?

$$
\langle E_\text{kin} \rangle = \frac{\langle p^2 \rangle}{2m} = \frac{\langle p \rangle^2}{2m} + \frac{\sigma_p^2}{2m} 
$$

The first term is energy associated with classical motion, and the second comes from quantum fluctuations.

So if all of the oscillator eigenstates are stationary, how can we get them to "move"?  Suppose we prepare a superposition of two such states, like $|\Psi(t=0)\rangle=\frac{1}{\sqrt{2}}(|0\rangle + |1\rangle)$. This will evolve in time as 

$$
|\Psi(t)\rangle= \frac{1}{\sqrt{2}}(e^{-iE_0t/\hbar}|0\rangle + e^{-iE_1t/\hbar}|1\rangle).
$$
The relative phase between the two states will cause the observable $\langle x(t) \rangle$ to oscillate in time.

## Coherent states

Another very important state of the quantum harmonic oscillator is the "coherent state" (see problem 3.35 from Griffiths).  Mathematically, the coherent state is defined as

$$
|\alpha\rangle = e^{-\frac{|\alpha|^2}{2}} \sum_{n=0}^{\infty} \frac{\alpha^n}{\sqrt{n!}} |n\rangle
$$

where $\alpha$ is complex. The coherent state has the following properties:

- The coherent state is *not* an eigenstate of the Hamiltonian. Therefore, generally, it will "move" (i.e., $\langle x \rangle$ and $\langle p \rangle$ oscillate in time).
- Instead, the coherent state is an eigenstate of the annihilation operator $\hat{a}$.

???+ note 
    Recall that the creation operator $\hat{a}^\dagger$ and the annihilation operator $\hat{a}$ have the following properties:
    $$
    \begin{align}
    \hat{a}|n\rangle = & \sqrt{n}|n-1\rangle \quad \text{and} \quad \hat{a}|0\rangle=0, \\
    \hat{a}^\dagger|n\rangle = & \sqrt{n+1}|n+1\rangle,\\
    \hat{a}^\dagger\hat{a}|n\rangle =&  n|n\rangle.
    \end{align}
    $$
    The last operator, $\hat{n}=\hat{a}^{\dagger}\hat{a}$, is called the number operator. It allows us to write the Hamiltonian as $\hat{H}=\hbar\omega(\hat{a}^\dagger\hat{a}+0.5)$. Comparing this to 
    $$
    \hat{H}=\frac{\hat{p}^2}{2m}+\frac{1}{2}m\omega_0^2\hat{x}^2,
    $$
    we can derive
    $$
    \begin{align}
    \hat{x} = &x_\text{ZPF}(\hat{a}^\dagger+\hat{a})\\
    \hat{p} = & i\frac{\hbar}{2 x_\text{ZPF}}(\hat{a}^\dagger-\hat{a}).
    \end{align}
    $$


- There are an infinite number of possible coherent states, since $\alpha$ can vary continuously: $\alpha = |\alpha|e^{i \theta}$.
- All coherent states are Heisenberg-limited minimum uncertainty wavepackets that satisfy $\sigma_x \sigma_p = \frac{\hbar}{2}$ (see exercise).
- As a function of time, a coherent state evolves into a new coherent state with the same amplitude but a different phase: $\alpha(t) = e^{-i \omega t} \alpha$.  
- The ground state $|0\rangle$ is also a coherent state, with $\alpha = 0$ and $\langle x \rangle = \langle p \rangle = 0$.

By expressing the operators for position and momentum in terms of creation and annihilation operators, it follows that a coherent state $\alpha$ has:

$$
\begin{align}
\langle x \rangle &= x_{ZPF} (\alpha + \alpha^*) = 2 x_{ZPF} \mathcal{Re}(\alpha)\\
\langle p \rangle &= \frac{\hbar}{2 x_{ZPF}} \big( \frac{\alpha - \alpha^*}{i} \big) = \frac{\hbar}{x_{ZPF}}\mathcal{Im}(\alpha)
\end{align}
$$

Using the fact that $\alpha(t) = e^{-i \omega t} \alpha(0)$, we can see that the above expectation values oscillate in time.  

![](figures/4/alpha.PNG){: style="width:300px"}

In quantum optics, $\hat{x}$ and $\hat{p}$ are called quadratures.

The photon number operator $\hat{n}$ is $\hat{a}^\dagger \hat{a}$ (i.e. $\langle n | \hat{n} | n \rangle = n$), and the Hamiltonian then becomes $\hat{H} = \hbar \omega (\frac{1}{2} + \hat{a}^\dagger \hat{a})$.  Using this, we find that

$$
\langle \alpha | \hat{n} | \alpha \rangle = |\alpha|^2 = \bar{n}
$$

It turns out that the variance of the number of photons in a coherent state, $\sigma_N^2$, is $|\alpha|^2$.  The property that the variance is equal to the mean tells us that coherent states are Poissonian.  If you recall that lasers are also described by Poissonian statistics, then we have found that lasers are actually just coherent states!

Coherent states have maximal "classical" energy, and even though they are (infinitely) large quantum superpositons, in some sense they are the most classical quantum states possible.


## Conclusions

- The expectation values of observables of stationary states do not depend on time. 
- However, there are quantum fluctuations. These are related to the spread/curvature of the wavefunction.
- The coherent state is not a stationary state. Its time dynamics resemble that of a classical particle. 
- The coherent state is an eigenstate of the annihilation operator.
- The expectation values for position and momentum of the coherent state $|\alpha\rangle$ can be expressed in terms of $\alpha$.   

#  Homework Lecture 4

## Exercise 1

Consider the following  quantum state of the Harmonic oscillator:

$$
|\psi\rangle = 
\frac{
|0\rangle + 
|1\rangle}
{\sqrt{2}}
$$

Calculate the expectation value $\langle E \rangle$, and the value of the "classical" energy: 

$$
E_{classical} = 
\frac{1}{2} m \omega_0^2 
\langle x \rangle^2 +
\frac{1}{2m} 
\langle p \rangle^2
$$

How much of the energy of this quantum superposition state is "classical"?

<!-- #region -->
## Exercise 2

Consider the coherent state of the harmonic oscillator is defined by:

$$
|\alpha\rangle =  
\sum_{n=0}^{\infty}
\frac{
e^{-|\alpha|^2/2} \alpha^n}
{\sqrt{n!}
}
|n\rangle
$$

where $\alpha$ is a complex number. 


**(a)** Show that the photon number of the coherent state follows a Poisson distribution with average photon number $\langle n \rangle = |\alpha|^2$.


**(b)** Show that a coherent state at $t=0$ with $\alpha(t=0) = \alpha_0$ evolves into a coherent state at time $t$ with $\alpha(t) = e^{-i\omega t} \alpha_0$. 

**(c)** Calculate the value of $\langle x \rangle$ and $\langle p \rangle$ for the coherent state.

**(d)** Show that the coherent state satisfies the minimum requirement of the Heisenberg uncertainty principle. 

<!-- #endregion -->
