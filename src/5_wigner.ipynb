{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas\n",
    "\n",
    "from matplotlib import pyplot\n",
    "import numpy as np\n",
    "from scipy.optimize import curve_fit\n",
    "from scipy.integrate import quad\n",
    "import plotly.offline as py\n",
    "import plotly.graph_objs as go\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lecture 5: The Wigner Quasi-Probability Distribution\n",
    "!!! success \"Expected prior knowledge\"\n",
    "\n",
    "    Before the start of this lecture, you should be able to:\n",
    "\n",
    "    - analyze the properties of the coherent state wavefunction   \n",
    "    - recall that $\\rho(x) = \\Psi(x)^*\\Psi(x)$ describes a probability distribution\n",
    "\n",
    "\n",
    "!!! summary \"Learning goals\"\n",
    "\n",
    "    After this lecture you will be able to:\n",
    "\n",
    "    - Describe the main properties of the Wigner function\n",
    "    - Describe the relation between the Wigner function and the probability distribution of a quantum state \n",
    "    \n",
    "\n",
    "In the last lecture we discussed some properties of quantum fluctuations of the ground state of the harmonic oscillator, and took a look at an interesting state of the quantum harmonic oscillator known as the coherent state.\n",
    "\n",
    "We will now inspect the coherent state in more detail, but first we will study a different way of representing the wavefunction called the Wigner quasiprobability distribution, or for short, the Wigner function.  \n",
    "\n",
    "For a single particle in one dimension in quantum state with wavefunction $\\psi(x)$, the Wigner function is defined as \n",
    "\n",
    "$$\n",
    "W(x,p) = \\frac{1}{\\pi \\hbar} \\int_{-\\infty}^{+\\infty} \\psi^*(x+y) \\psi(x-y) e^{2ipy/\\hbar} dy\n",
    "$$\n",
    "\n",
    "where $p$ is just the 1D scalar momentum, not the momentum operator.  We can also form $W(x,p)$ with $\\phi(p)$, the Fourier transform of $\\psi(x)$:\n",
    "\n",
    "$$\n",
    "W(x,p) = \\frac{1}{\\pi \\hbar} \\int_{-\\infty}^{+\\infty} \\phi^*(p+q) \\phi(p-q) e^{-2iqx/\\hbar} dq\n",
    "$$\n",
    "\n",
    "How should we interpret the Wigner function?  It must be something like a probability distribution, as the name suggests.  If we integrate $W(x,p)$ over all $p$, we get $|\\psi(x)|^2$:\n",
    "\n",
    "$$\n",
    "\\int W(x,p) dp = |\\psi(x)|^2\n",
    "$$\n",
    "\n",
    "which *is* a probability distribution.  Similarly, if we integrate over all $x$, we get $|\\phi(p)|^2$.\n",
    "\n",
    "$$\n",
    "\\int W(x,p) dx = |\\phi(p)|^2 \n",
    "$$\n",
    "\n",
    "So it's close, but there are two problems with calling $W(x,p)$ a probability distribution:\n",
    "- In quantum mechanics, we can only measure one thing at a time, and after measuring our wave function, it collapses, so $W(x,p)$ *cannot* be the probability of measuring both $x$ and $p$, which are non-commuting observables\n",
    "- for some interesting quantum states, $W(x,p)$ can be negative, and since probabilities cannot be negative, we must call it a quasiprobability distribution instead\n",
    "\n",
    "Note that although $W(x,p)$ can be negative, it must be constructed such that its integrals over $x$ and $p$ are positive, since these do indeed represent probability distributions, as shown above.\n",
    "\n",
    "Also note that the Wigner function is real-valued, though it may not be obvious from the formula above.\n",
    "\n",
    "To make this more concrete, let's calculate the Wigner function of the ground state of the harmonic oscillator:\n",
    "\n",
    "$$\n",
    "\\psi_0(x) = \\bigg( \\frac{m\\omega}{\\pi\\hbar} \\bigg)^{\\frac{1}{4}} e^{\\frac{-m\\omega x^2}{2\\hbar}}\n",
    "$$\n",
    "\n",
    "Defining $l^2 = \\frac{\\hbar}{m\\omega} = 2x_{ZPF}^2$, we can write\n",
    "\n",
    "$$\n",
    "\\psi_0(x) = \\bigg( \\frac{1}{\\pi l^2} \\bigg)^{\\frac{1}{4}} e^{\\frac{-x^2}{2 l^2}}\n",
    "$$\n",
    "\n",
    "Plugging into the Wigner function, we get\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "W(x,p) &= \\frac{1}{\\pi \\hbar} \\int e^{2ipy/\\hbar} \\psi^*(x+y) \\psi(x-y) dy\\\\\n",
    "&= \\frac{1}{\\pi \\hbar} \\bigg( \\frac{1}{\\pi l^2} \\bigg)^{\\frac{1}{2}} \\int e^{2ipy/\\hbar} e^{-(x+y)^2/2l^2} e^{-(x-y)^2/2l^2} dy\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "After working out the exponents, we get\n",
    "\n",
    "$$\n",
    "W(x,p) = \\frac{1}{\\pi \\hbar} \\bigg( \\frac{1}{\\pi l^2} \\bigg)^{\\frac{1}{2}} e^{-x^2 / l^2} \\int e^{2ipy/\\hbar} e^{-y^2 / l^2} dy\n",
    "$$\n",
    "\n",
    "which is just the Fourier transform of a Gaussian, which is still a Gaussian.  Thus, we have\n",
    "\n",
    "$$\n",
    "W(x,p) = \\frac{1}{\\pi \\hbar} e^{-x^2 / l^2} e^{-l^2 p^2 / \\hbar^2}\n",
    "$$\n",
    "\n",
    "This is a two-dimensional Gaussian \"disk\" centered around $x,p=0$;\n",
    "\n",
    "![](figures/5/wigner_plane.PNG){: style=\"width:300px\"}\n",
    "\n",
    "In the next two sections of this lecture, we will explore the Wigner functions of different quantum states using a Python library called QuTiP.  The homework for this lecture is at the bottom of the last section (\"Wigner Functions of Mixed States\").\n",
    "\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "md,ipynb",
   "text_representation": {
    "extension": ".md",
    "format_name": "markdown",
    "format_version": "1.3",
    "jupytext_version": "1.10.1"
   }
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.5"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
